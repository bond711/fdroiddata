Provides:org.fdroid.k9
Categories:Internet
License:Apache2
Web Site:https://code.google.com/p/k9mail
Source Code:https://github.com/k9mail/k-9
Issue Tracker:https://code.google.com/p/k9mail/issues

Auto Name:K-9 Mail
Summary:Full-featured email client
Description:
E-mail client supporting multiple accounts, POP3, IMAP and Push IMAP.
Can do encryption if [[org.thialfihar.android.apg]] is installed.
Settings and account configurations can be exported so that they can be imported
easily if you are switching packages/signatures: a file manager will need to be
already installed to achieve this.

Note that there appears to be a bug in 4.802 which causes messages to not
be displayed properly if using the Dark theme. If you have problems with
not seeing message content, try switching to the Light theme.

If you would like to contribute financially to k9mail, consider donating to
the local SPCA (Societies for the prevention of cruelty to animals).

[https://github.com/k9mail/k-9/wiki/ReleaseNotes Major version notes]
.

Repo Type:git
Repo:https://github.com/k9mail/k-9.git

Build:4.201,16025
    commit=7834cdd8ca
    srclibs=Jutf7@20
    extlibs=jzlib/jzlib-1.0.7.tar.gz
    prebuild=chmod +x tools/build-beta && \
        ./tools/build-beta && \
        rm -f libs/jzlib*.jar libs/htmlcleaner*.jar libs/jutf*.jar && \
        mkdir -p tests/res/ && \
        unzip libs/htmlcleaner-2.2-all.zip && \
        cp -r $$Jutf7$$/src/main/java/com/ src/  && \
        tar xzf libs/jzlib-1.0.7.tar.gz && \
        mv jzlib-1.0.7/com/jcraft src/com/ && \
        rm -rf libs/jzlib* libs/htmlcleaner*.zip jzlib*
    build=$$MVN3$$ package -f HtmlCleaner/pom.xml && \
        mv HtmlCleaner/target/htmlcleaner-2.2.jar libs/ && \
        rm -rf HtmlCleaner

Build:4.409,17046
    commit=4.409
    srclibs=Jutf7@20
    extlibs=jzlib/jzlib-1.0.7.tar.gz
    prebuild=chmod +x tools/build-beta && \
        ./tools/build-beta && \
        rm -f libs/jzlib*.jar libs/htmlcleaner*.jar libs/jutf*.jar && \
        mkdir -p tests/res/ && \
        unzip libs/htmlcleaner-2.2-all.zip && \
        cp -r $$Jutf7$$/src/main/java/com/ src/ && \
        tar xzf libs/jzlib-1.0.7.tar.gz && \
        mv jzlib-1.0.7/com/jcraft src/com/ && \
        rm -rf libs/jzlib* libs/htmlcleaner*.zip jzlib*
    update=.,plugins/HoloColorPicker,plugins/Android-PullToRefresh/library,plugins/ckChangeLog/library,plugins/ActionBarSherlock/library
    target=android-17
    build=$$MVN3$$ package -f HtmlCleaner/pom.xml && \
        mv HtmlCleaner/target/htmlcleaner-2.2.jar libs/

Build:4.802,20003
    commit=4.802
    srclibs=Jutf7@20
    rm=libs/jzlib-1.0.7.jar,libs/htmlcleaner-2.2.jar,libs/jutf7-1.0.1-SNAPSHOT.jar,tests
    extlibs=jzlib/jzlib-1.0.7.tar.gz
    prebuild=unzip libs/htmlcleaner-2.2-all.zip && \
        tar xzf libs/jzlib-1.0.7.tar.gz && \
        echo 'source.dir=src;$$Jutf7$$/src;jzlib-1.0.7' >> ant.properties && \
        rm libs/jzlib-1.0.7.tar.gz libs/htmlcleaner-2.2-all.zip
    build=$$MVN3$$ package -f HtmlCleaner/pom.xml && \
        mv HtmlCleaner/target/htmlcleaner-2.2.jar libs/

Build:4.803,20004
    commit=4.803
    srclibs=Jutf7@20
    rm=libs/jzlib-1.0.7.jar,libs/htmlcleaner-2.2.jar,libs/jutf7-1.0.1-SNAPSHOT.jar,tests
    extlibs=jzlib/jzlib-1.0.7.tar.gz
    prebuild=unzip libs/htmlcleaner-2.2-all.zip && \
        tar xzf libs/jzlib-1.0.7.tar.gz && \
        echo 'source.dir=src;$$Jutf7$$/src;jzlib-1.0.7' >> ant.properties && \
        rm libs/jzlib-1.0.7.tar.gz libs/htmlcleaner-2.2-all.zip
    build=$$MVN3$$ package -f HtmlCleaner/pom.xml && \
        mv HtmlCleaner/target/htmlcleaner-2.2.jar libs/

Build:4.804,20005
    commit=4.804
    srclibs=Jutf7@20
    rm=libs/jzlib-1.0.7.jar,libs/htmlcleaner-2.2.jar,libs/jutf7-1.0.1-SNAPSHOT.jar,tests
    extlibs=jzlib/jzlib-1.0.7.tar.gz
    prebuild=unzip libs/htmlcleaner-2.2-all.zip && \
        tar xzf libs/jzlib-1.0.7.tar.gz && \
        echo 'source.dir=src;$$Jutf7$$/src;jzlib-1.0.7' >> ant.properties && \
        rm libs/jzlib-1.0.7.tar.gz libs/htmlcleaner-2.2-all.zip
    build=$$MVN3$$ package -f HtmlCleaner/pom.xml && \
        mv HtmlCleaner/target/htmlcleaner-2.2.jar libs/

Auto Update Mode:Version %v
Update Check Mode:RepoManifest/4.8-MAINT
Current Version:4.804
Current Version Code:20005

