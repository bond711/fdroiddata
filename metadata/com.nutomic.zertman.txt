Categories:System
License:GPLv3
Web Site:
Source Code:https://github.com/Nutomic/movecerts
Issue Tracker:https://github.com/Nutomic/movecerts/issues

Auto Name:Move Certs!
Summary:Resolve certificates warnings
Description:
Disable the "Network may be monitored" warnings that show up after installing
a certificate in one click.
.

Requires Root:Yes

Repo Type:git
Repo:https://github.com/Nutomic/movecerts.git

Build:1.0.1,2
    commit=310f60a53226737877eb443532c98cb737705b7f
    subdir=app
    submodules=yes
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:1.0.1
Current Version Code:2

